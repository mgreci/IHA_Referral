import { Referrals } from '../imports/api/referrals.js';

Future = Npm.require('fibers/future');

newReferralEmailData = {
  subject: "New Patient Referral",
  body: "<p>Hello,</p><p>This is an automated email from the IHA Referral App. A new patient has been referred. Please see below for details.</p>"
};

houseCallEmailData = {
  subject: "Patient Requests House Call",
  body: "<p>Hello,</p><p>This is an automated email from the IHA Referral App. A patient has requested a house call. Please see below for details.</p>"
};

const api_key = Meteor.settings.mailgun.api_key;
const domain = Meteor.settings.mailgun.domain;
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

const FROM = Meteor.settings.mailgun.from;
const TO = Meteor.settings.mailgun.to;

verifyHouseCallRequestFields = (newHouseCall) => {
  //check required fields
  if (newHouseCall.firstName == null || typeof newHouseCall.firstName != "string" || newHouseCall.firstName == "") {
    return {result: "error", message: "Patient first name is empty"};
  }
  if (newHouseCall.lastName == null || typeof newHouseCall.lastName != "string" || newHouseCall.lastName == "") {
    return {result: "error", message: "Patient last name is empty"};
  }
  if (newHouseCall.phoneNumber == null || typeof newHouseCall.phoneNumber != "string" || newHouseCall.phoneNumber == "") {
    return {result: "error", message: "Patient phone number is empty"};
  }
  //optional fields
  if (newHouseCall.dischargeDate != null && typeof newHouseCall.dischargeDate != "string") {
    return {result: "error", message: "Patient discharge date is not a string"};
  }
  if (newHouseCall.location != null && typeof newHouseCall.location != "string") {
    return {result: "error", message: "Patient location is not a string"};
  }
  //passed verification, return success
  return {result: "success"};
}

verifyNewReferralFields = function(newReferral) {
  //check required fields
  if (newReferral.firstName == null || typeof newReferral.firstName != "string" || newReferral.firstName == "") {
    return {result: "error", message: "New referral patient first name is empty"};
  }
  if (newReferral.lastName == null || typeof newReferral.lastName != "string" || newReferral.lastName == "") {
    return {result: "error", message: "New referral patient last name is empty"};
  }
  if (newReferral.phoneNumber == null || typeof newReferral.phoneNumber != "string" || newReferral.phoneNumber == "") {
    return {result: "error", message: "New referral patient phone number is empty"};
  }
  if (newReferral.referralName == null || typeof newReferral.referralName != "string" || newReferral.referralName == "") {
    return {result: "error", message: "New referral name is empty"};
  }
  if (newReferral.referralPhoneNumber == null || typeof newReferral.referralPhoneNumber != "string" || newReferral.referralPhoneNumber == "") {
    return {result: "error", message: "New referral phone number is empty"};
  }
  //optional fields
  if (newReferral.dischargeDate == null || typeof newReferral.dischargeDate != "string") {
    return {result: "error", message: "New referral patient discharge date is not a string"};
  }
  if (newReferral.location == null || typeof newReferral.location != "string") {
    return {result: "error", message: "New referral patient location is not a string"};
  }
  if (newReferral.referralOrg == null || typeof newReferral.referralOrg != "string") {
    return {result: "error", message: "New referral organization is not a string"};
  }
  //verification passed, return success
  return {result: "success"};
}

formatData = function(data, type) {
  var formattedData = {};
  formattedData['subject'] = type == "referral" ? newReferralEmailData.subject : houseCallEmailData.subject;
  formattedData['body'] = type == "referral" ? newReferralEmailData.body : houseCallEmailData.body;

  formattedData['createdAt'] = data.createdAt;
  formattedData['patientName'] = data.firstName + " " + data.lastName;
  formattedData['patientPhoneNumber'] = data.phoneNumber;
  formattedData['patientDischargeDate'] = type == "referral" ? (data.dischargeDate != "" ? data.dischargeDate : "No Data") : "N/A";
  formattedData['patientLocation'] = type == "referral" ? (data.location != "" ? data.location : "No Data") : "N/A";
  formattedData['referralOrg'] = type == "referral" ? (data.referralOrg != "" ? data.referralOrg : "No Data") : "N/A";
  formattedData['referralName'] = type == "referral" ? data.referralName : "N/A";
  formattedData['referralPhoneNumber'] = type == "referral" ? data.referralPhoneNumber : "N/A";

  return formattedData;
};

sendEmail = function(userData, type) {
  const data = formatData(userData, type);

  const finalHTML = `
    ${data.body}
    <ul style='list-style:none'>
      <li><strong>Created At:</strong> ${data.createdAt} </li>
      <li><strong>Patient Name:</strong> ${data.patientName}</li>
      <li><strong>Patient Phone Number:</strong> ${data.patientPhoneNumber}</li>
      <li><strong>Estimated Discharge Date:</strong> ${data.patientDischargeDate} </li>
      <li><strong>Patient Current Location:</strong> ${data.patientLocation} </li>
      <li><strong>Referral Organization:</strong> ${data.referralOrg} </li>
      <li><strong>Referral Name:</strong> ${data.referralName} </li>
      <li><strong>Referral Phone Number:</strong> ${data.referralPhoneNumber} </li>
    </ul>
  `;

  var emailData = {
    from: FROM,
    to: TO,
    subject: data['subject'],
    html: finalHTML
  };

  if (enableEmail) {
    mailgun.messages().send(emailData, function (error, body) {
      if (error) {
        console.log(error);
      } else {
        console.log(body);
      }
    });
  } else {
    console.log("sendEmail: to = " + emailData.to + ", from = " + emailData.from + ", subject = " + emailData.subject + ", html = " + finalHTML);
  }
};

isSystemAdmin = function(userId) {
  if (!userId || typeof userId != "string") {
    return false;
  }
  const user = Meteor.users.findOne(userId);
  return user && user['sysAdmin'] == true;
};

Meteor.methods({
  /* createReferral is open to the public */
  createReferral : function(newReferral) {
    //verify referral data
    result = verifyNewReferralFields(newReferral);
    if (result.result != "success") {
      return result;
    }

    //add 'createdAt' property
    newReferral['createdAt'] = new Date();

    //insert into database
    Referrals.insert(newReferral, function(error) {
      if (error) {
        return error;
      }
    });

    //send email
    sendEmail(newReferral, "referral");

    //send success to client
    return {result: "success"};
  },
  /* removeReferral can only be used by system administrators */
  removeReferral : function(referralId) {
    if (isSystemAdmin(this.userId)) {
      if (referralId && typeof referralId == "string") {
        future = new Future();
        Referrals.remove(referralId, function(error, result) {
          if (error) {
            future.return({result: "error", message: error.reason});
          } else {
            future.return({result: "success"});
          }
        });
        return future.wait();
      } else {
        console.log(new Date());
        console.log("user: " + this.userId + " attempted to remove referral with invalid ID ", referralId);
        return {result: "error", message: "Referral ID is invalid"};
      }
    } else {
      console.log(new Date());
      console.log("user: " + this.userId + " attempted to remove referral '" + referralId + "' and is not authorized");
      return {result: "error", message: "User is not authorized to remove referral"};
    }
  },
  /* createReferralAPI is open to the public */
  createReferralAPI : function(newReferral) {
    //verify referral data
    result = verifyNewReferralFields(newReferral);
    if (result.result != "success") {
      this.setHttpStatusCode(400);
      return result;
    }

    //add 'createdAt' property
    newReferral['createdAt'] = new Date();

    //add 'type' property
    newReferral['type'] = "Patient Referral";

    //insert into database
    Referrals.insert(newReferral, function(error) {
      if (error) {
        this.setHttpStatusCode(500);
        return error;
      }
    });

    //send email
    try {
      sendEmail(newReferral, "referral");
    } catch(error) {
      // ignore any errors for now and let client know all is fine
      console.log("WARING: Unable to send email.");
      console.log(error);
    }

    //send success to client
    this.setHttpStatusCode(200);
    return {result: "success"};
  },
  /* requestHouseCallAPI is open to the public */
  requestHouseCallAPI : function(newHouseCall) {
    //verify house call data
    result = verifyHouseCallRequestFields(newHouseCall);
    if (result.result != "success") {
      //send 400 error to client
      this.setHttpStatusCode(400);
      return result;
    }

    //add 'createdAt' property
    newHouseCall['createdAt'] = new Date();

    //add 'type' property
    newHouseCall['type'] = "House Call";

    //insert into database
    Referrals.insert(newHouseCall, function(error) {
      if (error) {
        //send 500 error to client
        this.setHttpStatusCode(500);
        return error;
      }
    });

    //send email
    try {
      sendEmail(newHouseCall, "houseCall");
    } catch(error) {
      // ignore any errors for now and let client know all is fine
      console.log("WARING: Unable to send email.");
      console.log(error);
    }

    this.setHttpStatusCode(200);
    //send success to client
    return {result: "success"};
  },
});
