import { Referrals } from '../imports/api/referrals.js';

Meteor.publish('referrals', function() {
  if (isSystemAdmin(this.userId)) {
    return Referrals.find({}, {sort: {"createdAt": 1}});
  } else {
    return [];
  }
});
