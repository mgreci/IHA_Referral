#!/bin/bash

# Mailgun settings
API_KEY=key-71e0332934e1947e7c0ff0b46ad6cfce
DOMAIN="greci.tech"

# From and To
FROM_USER="iha.referral.app"
TO_ADDR="grecimatthew@gmail.com"

# Make the request
curl --user "api:$API_KEY" https:/api.mailgun.net/v3/$DOMAIN/messages \
-F from="IHA Referral App <$FROM_USER@$DOMAIN>" \
-F to=$TO_ADDR \
-F subject="New Patient Referral" \
-F text="This is a notice that a patient has been referred.

Name: Hello World
Date: 06/03/2018
Referral Name: Jon Doe"
