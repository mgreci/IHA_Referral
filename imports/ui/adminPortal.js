import './adminPortal.html';

import { Referrals } from '../api/referrals.js';

Template.adminPortal.events({
  'click #logout' : function() {
    Meteor.logout(function(error) {
      if (error) {
        swal({
          title: "Error!",
          text: "Unable to logout user: " + error.message,
          type: "error"
        });
      } else {
        //FlowRouter.go("/login");
        //console.log("logout");
      }
    });
  }
});

Template.listReferrals.onCreated(function() {
  Meteor.subscribe('referrals');
});

Template.userNotLoggedIn.events({
  'click button#login': function(event, template) {
    const email = $("input.login#email").val();
    const password = $("input.login#password").val();
    Meteor.loginWithPassword(email, password, function(error) {
      if (error) {
        swal({
          title: "Error!",
          text: "Unable to login in user: " + error.message,
          type: "error"
        });
      } else {
        //FlowRouter.go("/approval");
        //console.log("Successful login. Do you see referrals?");
      }
    });
  },
  'click button#signup': function(event, template) {
    const options = {
      email : $("input.signup#email").val(),
      password : $("input.signup#password").val()
    };
    Accounts.createUser(options, function(error) {
      if (error) {
        swal({
          title: "Error!",
          text: "Unable to create user: " + error.message,
          type: "error"
        });
      } else {
        //FlowRouter.go("/approval");
        //console.log("Successful login? Do you see referrals?");
      }
    });
  }
});

Template.listReferrals.events({
  'click #expandAll' : function() {
    $('.collapse').collapse('show');
  },
  'click #collapseAll' : function() {
    $('.collapse').collapse('hide');
  },
  'click button.removeReferral' : function(event) {
    const referralId = event.target.id;
    swal({
      title: "Are you sure?",
      text: "This will remove this referral permanently.",
      type: "warning",
      showCancelButton: true,
      showConfirmButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      closeOnConfirm: false
    }, function() {
      Meteor.call('removeReferral', referralId, function(error, response) {
        if (error) {
          swal("Error!", error, "error");
        } else if (response) {
          if (response.result != "success") {
            swal("Error!", "Error removing referral: " + response.message, "error");
          } else {
            swal("Success!", "Referral successfully removed.", "success");
          }
        }
      });
    });
  }
});

Template.listReferrals.helpers({
  nonZeroReferrals : function() {
    return Referrals.find().count() > 0;
  },
  referrals : function() {
    return Referrals.find().fetch();
  },
  humanIndex : function(index) {
    return index + 1;
  },
  patientName : function(firstName, lastName) {
    return firstName && lastName ? firstName + " " + lastName : "No Data";
  },
  humanPhoneNumber : function(phoneNumberRawString) {
    if (!phoneNumberRawString) {
      return "No Data";
    }
    if (typeof phoneNumberRawString != "string" || phoneNumberRawString.length != 10) {
      return "Invalid number '" + phoneNumberRawString + "'";
    }
    return phoneNumberRawString.substring(0, 3) + "-" + phoneNumberRawString.substring(3, 6) + "-" + phoneNumberRawString.substring(6);
  },
  patientDischargeDate : function(dischargeDate) {
    return dischargeDate ? dischargeDate.toLocaleString() : "No Data";
  },
  patientLocation : function(location) {
    return location ? location : "No Data";
  },
  referrerFullName : function(referralName) {
    return referralName ? referralName : "No Data";
  },
  referrerOrg : function(referralOrg) {
    return referralOrg ? referralOrg : "No Data";
  },
  createdAtString : function(createdAt) {
    return createdAt ? createdAt.toLocaleString() : "No Data";
  }
});
