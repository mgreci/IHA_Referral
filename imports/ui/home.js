import './home.html';

referralPassesVerification = function(newReferral) {
  var passVerification = true;
  //remove error class on all inputs
  $('.referralData').removeClass('error');
  //check data for required fields
  if (newReferral.firstName == "") {
    $('#firstName').attr("placeholder", "Patient first name is required").addClass('error');
    passVerification = false;
  }
  if (newReferral.lastName == "") {
    $('#lastName').attr("placeholder", "Patient last name is required").addClass('error');
    passVerification = false;
  }
  if (newReferral.phoneNumber == "") {
    $('#phoneNumber').attr("placeholder", "Patient phone number is required").addClass('error');
    passVerification = false;
  } else if (newReferral.phoneNumber.length != 10) {
    swal("Invalid Phone Number", "Patient phone number must be in correct format, i.e. 6191112222", "error");
    passVerification = false;
  }
  if (newReferral.referralName == "") {
    $('#referralName').attr("placeholder", "Referral name is required").addClass('error');
    passVerification = false;
  }
  if (newReferral.referralPhoneNumber == "") {
    $('#referralPhoneNumber').attr("placeholder", "Referral phone number is required").addClass('error');
    passVerification = false;
  } else if (newReferral.referralPhoneNumber.length != 10) {
    swal("Invalid Phone Number", "Referral phone number must be in correct format, i.e. 6191112222", "error");
    passVerification = false;
  }
  return passVerification;
}

Template.createReferral.onRendered(function() {
  $('.date').datepicker();
});

Template.createReferral.events({
  'click #submitReferral' : function() {
    //obtain data
    const newReferral = {
      firstName: $('input#firstName').val(),
      lastName: $('input#lastName').val(),
      phoneNumber: $('input#phoneNumber').val(),
      dischargeDate: $('input#dischargeDate').val(),
      location: $('input#location').val(),
      referralName: $('input#referralName').val(),
      referralOrg: $('input#referralOrg').val(),
      referralPhoneNumber: $('input#referralPhoneNumber').val(),
      source: "web-meteor"
    }

    if (!referralPassesVerification(newReferral)) {
      return;
    }

    //required fields are present, continue saving referral
    Meteor.call('createReferral', newReferral, function(error, response) {
      if (error) {
        swal("Oops!", error, "error");
      } else if (response) {
        if (response.result != "success") {
          swal("Error", "Error creating referral: " + response.message, "error");
        } else {
          //clear inputs
          $('.referralData').val('');
          $('.referralData').removeClass('error');
          //alert user success adding referral
          swal("Success", "Referral has been successfully added!", "success");
        }
      } else {
        //treat no response as an error
        swal("Error", "Error creating referral: no server response", "error");
      }
    });
  },
});
