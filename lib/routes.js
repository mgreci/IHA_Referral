import { FlowRouter } from 'meteor/kadira:flow-router';

FlowRouter.route('/', {
  name: 'home',
  action: function(params) {
    BlazeLayout.render("mainLayout", {main: "home"});
  }
});

FlowRouter.route('/mobile', {
  name: 'mobile',
  action: function(params) {
    BlazeLayout.render("mainLayout", {main: "mobile"});
  }
});

FlowRouter.route('/adminPortal', {
  name: 'adminPortal',
  action: function(params) {
    //console.log("Meteor.userId() = ", Meteor.userId());
    BlazeLayout.render("mainLayout", {main: "adminPortal"});
  }
});
